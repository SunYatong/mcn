Dataset=ml-100k, numFactor=32, in=5, tar=1

                                              AUC         Precision       Recall      NDCG        lr      lam     dp_keep     rnn_unit       filter  capsule
1   CapsuleConv(None)                         0.7941      0.0589          0.5887      0.3835      filter_num=4, capsule_num: 4, 4, 4
1   CapsuleConv(CNN+Pooling)                  0.7957      0.0592          0.5920      0.3854
1   CapsuleConv(CNN+Pooling cost)             0.7967      0.0594          0.5940      0.3875
1   CapsuleConv(CNN+Pooling point)            0.7967      0.0594          0.5940      0.3909
1   CapsuleConv(CNN+Pooling bias)


11  Caser                                     0.7586      0.0518          0.5177      0.3102      0.001   0.01    0.8                        0.001           0.7
12  GRU4Rec                                   0.7801      0.0561          0.5608      0.3644      0.001           0.5         64             seq_len=5
13  RUM(I)                                    0.7684      0.0537          0.5373      0.3256      0.001   0.01    0.8
    FPMC                                      0.7544      0.0509          0.5094      0.2788
    BPR                                       0.7435      0.0487          0.4872      0.2880
    ItemKNN                                   0.6316      0.0263          0.2633      0.1826
    UserKNN                                   0.7718      0.0543          0.5437      0.3501

    Amazon-electronics

               AUC         Precision       Recall      NDCG
    MCN        0.8089      0.0618          0.6183      0.4085      routing=1,  num_capsule: user-item: 1, item-item: 2, preference: 1
    CCS        0.7967      0.0594          0.5940      0.3909
    Caser      0.7586      0.0518          0.5177      0.3102      0.001   0.01    0.8                        0.001           0.7
    GRU4Rec    0.7801      0.0561          0.5608      0.3644      0.001           0.5         64             seq_len=5
    RUM(I)     0.7684      0.0537          0.5373      0.3256      0.001   0.01    0.8
    FPMC       0.7544      0.0509          0.5094      0.2788
    BPR        0.7435      0.0487          0.4872      0.2880
    CoupledCF  0.7336      0.0468          0.4677      0.2747      filter_shapes:[3, 4, 5, 6, 7], fc_dim:[32, 16], dropkeep:0.8
    NCF        0.7331      0.0467          0.4667      0.2784      neg_sample_num=5, dropout_keep=0.2, fc_dim=[32, 8], pred_mlp_dim=[24, 8]
    ItemKNN    0.6316      0.0263          0.2633      0.1826
    UserKNN    0.7718      0.0543          0.5437      0.3501







\begin{table*}[tbp]
\fontsize{6}{6}\selectfont
\centering
\small
\caption{Performance comparison with other models, where * indicates the best performance among all baseline methods. The best performance for each setting is boldfaced. }
\label{tab:whole-result}
\begin{tabular}{l|l|ccccccc}
\toprule
\textbf{Dataset}               & \textbf{Metrics} & \textbf{ItemKNN} & \textbf{BPR} & \textbf{FPMC}  & \textbf{RUM-I} & \textbf{GRU4Rec} & \textbf{Caser} & \textbf{CCS} \\
\midrule
\multirow{4}{*}{movielens}     & Precision        & 0.0348           & 0.0608       & 0.0672         & 0.0695         & 0.0700           & 0.0703*         & \textbf{0.0738}          \\
                               & Recall           & 0.3487           & 0.6083       & 0.6720         & 0.6954         & 0.6997           & 0.7029*         & \textbf{0.7380}           \\
                               & AUC              & 0.6734           & 0.8024       & 0.8321         & 0.8440         & 0.8460           & 0.8476*         & \textbf{0.8649}           \\
                               & NDCG             & 0.1737           & 0.3332       & 0.3789         & 0.4114         & 0.4155           & 0.4191*         & \textbf{0.4347}           \\ \hline

\multirow{4}{*}{elect}         & Precision        & 0.0263           & 0.0487       & 0.0509         & 0.0537         & 0.0561*           & 0.0518         & \textbf{0.0594}           \\
                               & Recall           & 0.2633           & 0.4872       & 0.5094         & 0.5373         & 0.5608*           & 0.5177         & \textbf{0.5940}           \\
                               & AUC			  & 0.6316           & 0.7435       & 0.7544         & 0.7684         & 0.7801*           & 0.7586         & \textbf{0.7967}           \\
                               & NDCG             & 0.1826           & 0.2880       & 0.2788         & 0.3256         & 0.3256*           & 0.3102         & \textbf{0.3909}           \\ \hline

\multirow{4}{*}{kindle}        & Precision        & 0.0302           & 0.0354       & 0.0719        & 0.0741*          & 0.0685           & 0.0678         & \textbf{0.0778}           \\
                               & Recall           & 0.3022           & 0.3540       & 0.7192        & 0.7405*          & 0.6845           & 0.6781         & \textbf{0.7778}           \\
                               & AUC              & 0.6510           & 0.6769       & 0.8593        & 0.8701*          & 0.8421           & 0.8389         & \textbf{0.8887}           \\
                               & NDCG             & 0.1192           & 0.1916       & 0.4547        & 0.5347*          & 0.4890           & 0.3775         & \textbf{0.5353}           \\ \hline
\bottomrule
\end{tabular}
\end{table*}


\begin{table*}[tbp]
\fontsize{6}{6}\selectfont
\centering
\small
\caption{Performance comparison with variants of CCS model. The best performance for each setting is boldfaced. }
\label{tab:variants-result}
\begin{tabular}{l|l|ccccc}
\toprule
\textbf{Dataset}               & \textbf{Metrics} & \textbf{raw} & \textbf{cnn} & \textbf{cnn+point}  & \textbf{cnn+bias} & \textbf{cnn+cost}  \\
\midrule
\multirow{4}{*}{movielens}     & Precision        & 0.0715           & 0.0721       & 0.0733         & 0.0693                  & \textbf{0.0738}          \\
                               & Recall           & 0.7146           & 0.7210       & 0.7327         & 0.6933                  & \textbf{0.7380}           \\
                               & AUC              & 0.8533           & 0.8565       & 0.8623         & 0.8427                  & \textbf{0.8649}           \\
                               & NDCG             & 0.4228           & 0.4246       & 0.4328         & 0.4067                  & \textbf{0.4347}           \\ \hline

\multirow{4}{*}{elect}         & Precision        & 0.0589           & 0.0592       & \textbf{0.0594}         & 0.0590                  & 0.0594           \\
                               & Recall           & 0.5887           & 0.5920       & \textbf{0.5940}         & 0.5905                  & 0.5940           \\
                               & AUC			  & 0.7941           & 0.7957       & \textbf{0.7967}         & 0.7950                  & 0.7967           \\
                               & NDCG             & 0.3835           & 0.3854       & \textbf{0.3909}         & 0.3887                  & 0.3875           \\ \hline

\multirow{4}{*}{kindle}        & Precision        & 0.0767           & 0.0772       & \textbf{0.0778}        & 0.0770                  & 0.0775           \\
                               & Recall           & 0.7673           & 0.7718       & \textbf{0.7778}        & 0.7699                  & 0.7746           \\
                               & AUC              & 0.8834           & 0.8857       & \textbf{0.8887}        & 0.8847                  & 0.8871           \\
                               & NDCG             & 0.5166           & 0.5276       & \textbf{0.5353}        & 0.5312                  & 0.5309           \\
\bottomrule
\end{tabular}
\end{table*}


\begin{algorithm}[tb]
\caption{Routing algorithm returns activation and pose of the capsules in layer M + 1 given the
activations and votes of capsules in layer M. $V_{ij}^{h}$ is the $h$th dimension of the vote from capsule $i$
with activation $a_{i}$ in layer $M$ to capsule $j$ in layer $M + 1$. $\beta_{a}$, $\beta_{b}$ are learned discriminatively and the
inverse temperature $\lambda$ increases at each iteration with a fixed schedule}
\label{algo:em-routing}

\begin{algorithmic}[1]

\Procedure{EM ROUTING(a, V)}{}
\State $\textit{stringlen} \gets \text{length of }\textit{string}$
\State $i \gets \textit{patlen}$
\BState \emph{top}:
\If {$i > \textit{stringlen}$} \Return false
\EndIf
\State $j \gets \textit{patlen}$
\BState \emph{loop}:
\If {$\textit{string}(i) = \textit{path}(j)$}
\State $j \gets j-1$.
\State $i \gets i-1$.
\State \textbf{goto} \emph{loop}.
\State \textbf{close};
\EndIf
\State $i \gets i+\max(\textit{delta}_1(\textit{string}(i)),\textit{delta}_2(j))$.
\State \textbf{goto} \emph{top}.
\EndProcedure

\end{algorithmic}
\end{algorithm}