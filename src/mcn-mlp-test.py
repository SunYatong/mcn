from recommender.MCN_mlp import MUIC_Recommender
from data_model.SequenceDataModel import SequenceDataModel

# os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
# os.environ[""]="0"


if __name__ == '__main__':

    # instant_video_5_core, digital_music_5_core_100, baby_5_core_100  apps_for_android_5 video_games_5

    config = {
        'test-info': 'MUIC-save_test',
        'generate_seq': True,
        'splitterType': 'userTimeRatio',
        'fileName': 'ml-100k',
        'trainType': 'test',
        'threshold': 0,
        'learnRate': 0.001,
        'maxIter': 15,
        'trainBatchSize': 939,  # 830,
        'testBatchSize': 939,  # 830,
        'numFactor': 16,
        'topN': 10,
        'factor_lambda': 0.01,
        'goal': 'ranking',
        'verbose': False,
        'input_length': 3,
        'target_length': 1,
        'dropout_keep': 0.8,
        'fc_dim': [256, 64, 32],
        'capsule_num': 1,
        'dynamic_routing_iter': 3,
        'eval_item_num': 100,
        'filter_num': 5,
        'early_stop': True,

        'save_model': False,
        'load_model': False,

        'random_seed': 1,
        'rating_threshold': 100,

        'item-weight-more': False,
        'corr-type': 'dot-product'
    }

    for fileName in ['ml-100k']:
        config['fileName'] = fileName

        dataModel = SequenceDataModel(config)
        dataModel.buildModel()

        for input_len in [5]:
            for target_len in [1]:

                for dim_1 in [32, 64, 128, 256]:
                    for dim_2 in [32, 64, 128, 256]:
                        for dim_3 in [32, 64, 128, 256]:
                            for dp_keep in [1.0, 0.8, 0.6, 0.4, 0.2]:
                                if dim_3 < dim_2 and dim_2 < dim_1:
                                    dataModel.generate_sequences_hor(input_len, target_len)
                                    config['input_length'] = input_len
                                    config['target_length'] = target_len
                                    config['dropout_keep'] = dp_keep
                                    config['fc_dim'][0] = dim_1
                                    config['fc_dim'][1] = dim_2
                                    config['fc_dim'][2] = dim_3
                                    recommender = MUIC_Recommender(dataModel, config)
                                    recommender.run()









