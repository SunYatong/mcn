from multiprocessing.pool import ThreadPool as Pool
import numpy as np
import time
import matplotlib.pyplot as plt
import os.path

class RankingEvaluator:

    def __init__(self, groundTruthLists, user_items_train, itemInTestSet, topK, testMatrix, conf):

        '''
        :param groundTruthLists: a dict {userId:[itemId1, itemId2, ...], ...}
        :param user_items_train: a dict {userId:[itemId1, itemId2, ...], ...}
        :param itemInTestSet: Integer
        :param topK:    Integer
        :param testMatrix: a dict {(userId, itemId):rating ...}
        '''

        self.groundTruthLists = groundTruthLists
        self.user_items_train = user_items_train
        self.predLists = None
        self.indexRange = len(self.groundTruthLists)
        self.itemInTestSet = itemInTestSet
        self.topK = topK
        self.testMatrix = testMatrix
        self.pool = Pool()
        self.diagram_count = 0
        self.conf = conf

        self.image_dir_path = os.path.join('./log', conf['test-info'] + str(
            time.strftime('%Y-%m-%d-%H-%M-%S', time.localtime(time.time()))))
        os.makedirs(self.image_dir_path)

    def setPredLists(self, predLists):
        self.predLists = predLists

    '''Recall'''
    def calRecall(self, topN):
        start = time.time()
        results = []

        for userIdx in self.groundTruthLists:
            results.append(self.pool.apply_async(self.calculate_a_Recall, (userIdx, topN)))

        recallSum = 0
        for result in results:
            recallSum += result.get()

        end = time.time()
        return recallSum / len(results), (end - start)

    def calculate_a_Recall(self, userIdx, topN):
        hitNum = 0
        userTrueList = self.groundTruthLists[userIdx]
        userPredList = self.predLists[userIdx][0:topN]

        for itemId in userPredList:
            if itemId in userTrueList:
                hitNum += 1

        return hitNum / len(userTrueList)

    '''Precision'''
    def calPrecision(self, topN):
        start = time.time()
        results = []

        for userIdx in self.groundTruthLists:
            results.append(self.pool.apply_async(self.calculate_a_Precision, (userIdx, topN)))

        precisionSum = 0
        for result in results:
            precisionSum += result.get()

        end = time.time()

        return precisionSum / len(results), (end - start)

    def calculate_a_Precision(self, userIdx, topN):
        hitNum = 0
        userTrueList = self.groundTruthLists[userIdx]
        userPredList = self.predLists[userIdx][0:topN]

        for itemId in userPredList:
            if itemId in userTrueList:
                hitNum += 1

        return hitNum / len(userPredList)

    '''AUC'''
    def calAUC(self, topN):
        start = time.time()
        results = []

        for userIdx in self.groundTruthLists:
            results.append(self.pool.apply_async(self.calculate_a_AUC, (userIdx, topN)))
        aucSum = 0
        for result in results:
            aucSum += result.get()

        end = time.time()

        return aucSum / len(results), (end - start)


    def calculate_a_AUC(self, userIdx, topN):
        userTrueList = self.groundTruthLists[userIdx]
        userPredList = self.predLists[userIdx][0:topN]

        numEval = len(self.itemInTestSet) - len(self.itemInTestSet.intersection(set(self.user_items_train[userIdx])))

        numRelevant = 0
        numMiss = 0
        for itemIdx in userTrueList:
            if itemIdx in userPredList:
                numRelevant += 1
            else:
                numMiss += 1

        if numEval == numRelevant:
            return 1.0

        numPairs = numRelevant * (numEval - numRelevant)

        if numPairs == 0:
            return 0.5

        numHits = 0
        numCorrectPairs = 0
        for itemIdx in userPredList:
            if itemIdx in userTrueList:
                numHits += 1
            else:
                numCorrectPairs += numHits

        numCorrectPairs += numHits * (numEval - len(userPredList) - numMiss)
        return numCorrectPairs / numPairs

    '''NDCG'''
    def calNDCG(self, topN):
        start = time.time()
        results = []

        for userIdx in self.groundTruthLists:
            results.append(self.pool.apply_async(self.calculate_a_NDCG, (userIdx, topN)))
        ndcgSum = 0
        for result in results:
            ndcgSum += result.get()

        end = time.time()

        self.print_ndcg_diagram()

        return ndcgSum / len(results), (end - start)


    def calculate_a_NDCG(self, userIdx, topN):
        ratingList = []
        userTrueList = self.groundTruthLists[userIdx]
        userPredList = self.predLists[userIdx][0:topN]

        # calculate DCG and build rating list
        DCG = 0.0
        for index in range(len(userPredList)):
            itemIdx = userPredList[index]
            if itemIdx in userTrueList:
                ratingValue = self.testMatrix[userIdx, itemIdx]
                ratingList.append(ratingValue)
                DCG += ratingValue / np.log2(index + 2)
        if len(ratingList) == 0:
            return 0

        # calculate IDCG
        ratingList.sort(reverse=True)
        IDCG = 0.0
        for index in range(len(ratingList)):
            ratingValue = ratingList[index]
            IDCG += ratingValue / np.log2(index + 2)

        assert IDCG >= DCG
        return DCG / IDCG

    def print_ndcg_diagram(self, ):
        pass
        # fig = plt.figure(frameon=False)
        # fig.set_size_inches(10000, 10)
        # xmajorLocator = plt.MultipleLocator(1)  # 将x主刻度标签设置为20的倍数
        # ax.xaxis.set_major_locator(xmajorLocator)
        # axes = plt.gca()
        # axes.set_ylim([0.0, 1.0])
        #
        # user_numItem = {}
        # user_ndcg = {}
        #
        # for userIdx in self.groundTruthLists:
        #     user_numItem[userIdx] = len(self.user_items_train[userIdx])
        #     ndcg = self.calculate_a_NDCG(userIdx)
        #     user_ndcg[userIdx] = ndcg
        #
        # userlist_numItem_ascend = sorted(user_numItem, key=user_numItem.__getitem__, reverse=False)
        # interval = 4
        # max_num = 100
        # bins = {}
        # for i in range(max_num // interval):
        #     bins[i] = []
        #
        # for userIdx in userlist_numItem_ascend:
        #     item_num = user_numItem[userIdx]
        #     if item_num < max_num:
        #         bins[item_num // interval].append(user_ndcg[userIdx])
        #
        # axis_x = []
        # axis_y = []
        # sizes = []
        # for i in range(max_num // interval):
        #     ndcgs = bins[i]
        #     if len(ndcgs) == 0:
        #         continue
        #     axis_x.append(i)
        #     ndcg_sum = 0
        #     for ndcg in ndcgs:
        #         ndcg_sum += ndcg
        #     sizes.append(len(ndcgs))
        #     axis_y.append(ndcg_sum / len(ndcgs))
        #
        # plt.title("ndcg-" + str(self.diagram_count))
        # self.diagram_count += 1
        #
        # plt.xlabel('numItem')
        # plt.ylabel('avg_ndcg')
        #
        # plt.scatter(axis_x, axis_y, sizes)
        # plt.savefig(self.image_dir_path + "/ndcg-" + str(self.diagram_count) + '.png')
        # plt.clf()
        # plt.show()



if __name__ == '__main__':
    user_items_train = {0: [0, 1, 2, 3, 4, 5],
                        1: [0, 1, 3, 6],
                        2: [0, 2, 3, 6]}
    groundTruthList = {
                       0: [7, 8, 9],
                       1: [7, 8, 9],
                       2: [7, 8, 9]
    }
    topK = 3
    numItem = set()
    numItem.add(4)
    numItem.add(5)
    numItem.add(7)
    numItem.add(8)
    numItem.add(9)

    testMatrix = {(1, 7): 5,
                  (1, 8): 4,
                  (1, 9): 3,
                  (0, 7): 5,
                  (0, 8): 4,
                  (0, 9): 3,
                  (2, 7): 5,
                  (2, 8): 4,
                  (2, 9): 3,
                  }
    predLists = {0: [7, 8, 9],
                 1: [7, 4, 5],
                 2: [4, 7, 8],
                 }
    evaluator = RankingEvaluator(
        groundTruthLists=groundTruthList,
        user_items_train=user_items_train,
        itemInTestSet=numItem,
        topK=topK,
        testMatrix=testMatrix)
    evaluator.setPredLists(predLists)
    print("AUC:" + str(evaluator.calAUC()))
    print("Recall:" + str(evaluator.calRecall()))
    print("Precision:" + str(evaluator.calPrecision()))
    print("NDCG:" + str(evaluator.calNDCG()))









