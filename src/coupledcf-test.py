
from recommender.CoupledCF import CoupledCFRecommender
from data_model.SequenceDataModel import SequenceDataModel

if __name__ == '__main__':

    # instant_video_5_core, digital_music_5_core_100, baby_5_core_100  apps_for_android_5 video_games_5

    config = {
        'test-info': 'test',
        'item-weight-more': False,
        'generate_seq': True,
        'splitterType': 'userTimeRatio',
        'fileName': 'ml-100k',
        'trainType': 'test',
        'threshold': 0,
        'learnRate': 0.001,
        'maxIter': 1000,
        'trainBatchSize': 939,
        'testBatchSize': 939,
        'numFactor': 16,
        'topN': 10,
        'factor_lambda': 0.1,
        'goal': 'ranking',
        'verbose': False,

        'neg_sample_num': 5,

        'fc_dim': [32, 16],
        'pred_mlp_dim': [16 * 16 + 16 + 5 * 5, 8],
        'eval_item_num': 100,
        'early_stop': True,
        'random_seed': 1,
        'save_model': False,
        'load_model': False,

        'filter_num': 1,
        'filter_heights': [1, 2, 3, 4, 5],
        'filter_widths': [1, 2, 3, 4, 5],
        'cnn_lambda': 0.01,
        'dropout_keep': 0.8,
    }

    for fileName in ['ml-100k']:
        config['fileName'] = fileName

        dataModel = SequenceDataModel(config)
        dataModel.buildModel()
        for drop_keep in [1.0, 0.8, 0.6, 0.4, 0.2]:
            for shape in [1, 2, 3, 4]:
                for filter_num in [1, 2, 3, 4]:
                    config['filter_heights'] = [shape, shape+1, shape+2, shape+3, shape+4]
                    config['filter_num'] = filter_num
                    config['dropout_keep'] = drop_keep
                    config['pred_mlp_dim'] = [16 * 16 + 16 + filter_num * 5, 8]

                    recommender = CoupledCFRecommender(dataModel, config)
                    recommender.run()


















