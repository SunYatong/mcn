import tensorflow as tf
from recommender.NCF import NCFRecommender
from component.CNN_2D import CNN_Pool_2d_Compoment

class CoupledCFRecommender(NCFRecommender):

    def __init__(self, dataModel, config):

        super(CoupledCFRecommender, self).__init__(dataModel, config)

        self.name = 'CoupledCF'
        self.CNN = CNN_Pool_2d_Compoment(
            filter_num=config['filter_num'],
            filter_heights=config['filter_heights'],
            filter_widths=config['filter_widths'],
            output_size=self.numFactor,
            cnn_lambda=config['cnn_lambda'],
            dropout_keep_prob=config['dropout_keep'],
            name='coupledcf-2d-cnn-'
        )


    def pred_with_user_item_id(self, userMF_emedding, userMLP_embed, itemId):

        itemMF_embedding = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, itemId), [-1, self.numFactor])
        mf_out_product = self.out_product(itemMF_embedding, userMF_emedding)

        cnn_output = self.CNN.get_conv_pool_output(input_image=mf_out_product)

        flatten_outproduct = tf.reshape(mf_out_product, shape=[-1, self.numFactor * self.numFactor])

        itemMLP_embed = tf.reshape(tf.nn.embedding_lookup(self.itemMLP_Embedding, itemId), [-1, self.numFactor])
        MLP_output = self.MLP.get_output(feature_input=tf.concat([userMLP_embed, itemMLP_embed], axis=1))

        cnn_flatten_mlp_concat = tf.concat([cnn_output, flatten_outproduct, MLP_output], axis=1)

        pred_embed = self.pred_MLP.get_output(feature_input=cnn_flatten_mlp_concat)

        pred_rating = tf.matmul(pred_embed, self.pred_weight)

        return pred_rating
