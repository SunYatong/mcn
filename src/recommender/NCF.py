import numpy as np
import tensorflow as tf
import random
from recommender.BasicRcommender import BasicRecommender
import time
from component.MLP import MLP

class NCFRecommender(BasicRecommender):

    def __init__(self, dataModel, config):

        super(NCFRecommender, self).__init__(dataModel, config)

        self.name = 'NCF'

        self.numFactor = config['numFactor']
        self.factor_lambda = config['factor_lambda']
        self.neg_sample_num = config['neg_sample_num']
        self.dropout_keep = config['dropout_keep']
        self.fc_dim = config['fc_dim']
        self.pred_mlp_dim = config['pred_mlp_dim']

        # placeholders
        self.u_id = tf.placeholder(tf.int32, [None, 1])
        self.pos_item = tf.placeholder(tf.int32, [None, 1])
        self.neg_items = tf.placeholder(tf.int32, [None, self.neg_sample_num])
        self.pred_item = tf.placeholder(tf.int32, [None, self.eval_item_num])
        self.dropout_keep_placeholder = tf.placeholder_with_default(1.0, shape=())

        # user/item mlp embedding
        weights_regularizer = tf.contrib.layers.l2_regularizer(config['factor_lambda'])
        with tf.variable_scope("mlp-factors-", reuse=None, regularizer=weights_regularizer):

            self.userMLP_Embedding = tf.get_variable('user_mlp_embedding', dtype=tf.float32,
                            initializer=tf.random_normal([self.numUser, self.numFactor], 0, 0.1))

            self.itemMLP_Embedding = tf.get_variable('item_mlp_embedding', dtype=tf.float32,
                            initializer=tf.random_normal([self.numItem, self.numFactor], 0, 0.1))

            self.pred_weight = tf.get_variable(name="pred_weight", shape=[self.pred_mlp_dim[-1], 1], dtype=tf.float32,
                                               initializer=tf.contrib.layers.xavier_initializer(),
                                               )
        self.item_bias = tf.get_variable(name="item_bias", dtype=tf.float32,
                                               initializer=tf.random_normal([self.numItem, 1], 0, 0.1),
                                               )

        self.MLP = MLP(self.fc_dim, dropout_keep=self.dropout_keep_placeholder, name='user-item')

        self.pred_MLP = MLP(self.pred_mlp_dim, dropout_keep=self.dropout_keep_placeholder, name='mf-mlp')


    def buildModel(self):
        with tf.variable_scope(tf.get_variable_scope()) as scope:

            userMF_emedding = tf.reshape(tf.nn.embedding_lookup(self.userEmbedding, self.u_id), [-1, self.numFactor])
            userMLP_embed = tf.reshape(tf.nn.embedding_lookup(self.userMLP_Embedding, self.u_id), [-1, self.numFactor])

            # get pos prediction
            pos_pred = self.pred_with_user_item_id(userMF_emedding, userMLP_embed, itemId=self.pos_item)
            pos_loss = - tf.reduce_mean(tf.log(tf.nn.sigmoid(pos_pred)))
            neg_loss = 0

            # get neg predictions
            split_list = [1] * self.neg_sample_num
            neg_item_Ids = tf.split(self.neg_items, split_list, 1)

            for neg_itemId in neg_item_Ids:
                neg_pred = self.pred_with_user_item_id(userMF_emedding, userMLP_embed, itemId=neg_itemId)
                neg_loss += - tf.reduce_mean(tf.log(1 - tf.nn.sigmoid(neg_pred)))
            self.cost = pos_loss + neg_loss

            # get eval predictions
            pred_split_list = [1] * self.eval_item_num
            pred_item_ids = tf.split(self.pred_item, pred_split_list, 1)
            preds = []
            for pred_item_id in pred_item_ids:
                pred = self.pred_with_user_item_id(userMF_emedding, userMLP_embed, itemId=pred_item_id)
                preds.append(pred)
            self.r_pred = tf.concat(preds, axis=1)


    def pred_with_user_item_id(self, userMF_emedding, userMLP_embed, itemId):

        itemMF_embedding = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, itemId), [-1, self.numFactor])
        mf_ele_product = tf.multiply(userMF_emedding, itemMF_embedding)

        itemMLP_embed = tf.reshape(tf.nn.embedding_lookup(self.itemMLP_Embedding, itemId), [-1, self.numFactor])
        MLP_output = self.MLP.get_output(feature_input=tf.concat([userMLP_embed, itemMLP_embed], axis=1))

        mf_mlp_concat = tf.concat([mf_ele_product, MLP_output], axis=1)

        pred_embed = self.pred_MLP.get_output(feature_input=mf_mlp_concat)

        pred_rating = tf.matmul(pred_embed, self.pred_weight)

        return pred_rating

    def trainEachBatch(self, epochId, batchId):
        totalLoss = 0
        start = time.time()
        user_batch, pos_seq_batch, neg_seq_batch = self.getTrainData(batchId)

        # print(batchId)

        self.optimizer.run(feed_dict={
            self.u_id: user_batch,
            self.pos_item: pos_seq_batch,
            self.neg_items: neg_seq_batch,
            self.dropout_keep_placeholder: self.dropout_keep
        })

        loss = self.cost.eval(feed_dict={
            self.u_id: user_batch,
            self.pos_item: pos_seq_batch,
            self.neg_items: neg_seq_batch,
            self.dropout_keep_placeholder: self.dropout_keep
        })
        totalLoss += loss
        end = time.time()
        if epochId % 5 == 0 and batchId == 0:
            self.logger.info("----------------------------------------------------------------------")
            self.logger.info(
                "batchId: %d epoch %d/%d   batch_loss: %.4f   time of a batch: %.4f" % (
                    batchId, epochId, self.maxIter, totalLoss, (end - start)))

            self.evaluateRanking(epochId, batchId)
        return totalLoss

    def getTrainData(self, batchId):
        # compute start and end
        start = time.time()
        user_batch = []
        pos_item_batch = []
        neg_item_batch = []
        for i in range(self.trainBatchSize):
            userIdx = random.randint(0, self.numUser - 2)
            positiveItems = self.user_items_train[userIdx]
            positiveItemIdx = positiveItems[random.randint(0, len(positiveItems) - 1)]

            neg_items = []
            for j in range(self.neg_sample_num):
                negativeItemIdx = random.randint(0, self.numItem - 1)
                while negativeItemIdx in positiveItems:
                    negativeItemIdx = random.randint(0, self.numItem - 1)
                neg_items.append(negativeItemIdx)

            user_batch.append(userIdx)
            pos_item_batch.append(positiveItemIdx)
            neg_item_batch.append(neg_items)

        users = np.array(user_batch).reshape((self.trainBatchSize, 1))
        pos_items = np.array(pos_item_batch).reshape((self.trainBatchSize, 1))
        neg_items = np.array(neg_item_batch).reshape((self.trainBatchSize, self.neg_sample_num))

        return users, pos_items, neg_items

    def getPredList_ByUserIdxList(self, user_idices):
        end0 = time.time()
        # build test batch
        target_seq = []

        for userIdx in user_idices:
            target_seq.append(self.evalItemsForEachUser[userIdx])

        batch_u = np.array(user_idices).reshape((-1, 1))
        target_seq = np.array(target_seq)

        end1 = time.time()

        predList = self.sess.run(self.r_pred, feed_dict={
            self.u_id: batch_u,
            self.pred_item: target_seq,
        })
        end2 = time.time()

        output_lists = []
        for i in range(len(user_idices)):
            recommendList = {}
            start = i * self.eval_item_num
            end = start + self.eval_item_num
            for j in range(end-start):
                recommendList[target_seq[i][j]] = predList[i][j]
            sorted_RecItemList = sorted(recommendList, key=recommendList.__getitem__, reverse=True)[0:self.topN]
            output_lists.append(sorted_RecItemList)
        end3 = time.time()

        return output_lists, end1 - end0, end2 - end1, end3 - end2
