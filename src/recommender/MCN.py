import tensorflow as tf
from recommender.CapSeq_Basic import CapSeqBasic
from component.Capsule import Capsule_Component
import os
import time
import numpy as np
import copy
import random
from util.dim_red import PCACapsule




class MUIC_Recommender(CapSeqBasic):

    def __init__(self, dataModel, config):

        super(MUIC_Recommender, self).__init__(dataModel, config)

        self.name = 'MCN'
        self.user_item_capsule_num = config['user_item_capsule_num']
        self.item_item_capsule_num = config['item_item_capsule_num']
        self.preference_capsule_num = config['preference_capsule_num']
        self.capsule_lambda = config['capsule_lambda']
        self.routing_type = config['routing_type']


        # Conv-Capsule Layers
        self.hor_pattern_capsules = []
        self.user_embeddings = []
        self.item_item_coupling = []
        self.user_item_coupling = []
        self.preference_coupling = []
        for i in range(self.numFactor):
            item_item_capsule = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=self.input_length,
                num_caps_j=self.item_item_capsule_num,
                in_vec_len=self.numFactor,
                out_vec_len=self.numFactor,
                user_vec_len=self.numFactor,
                user_bias=False,
                T=self.dynamic_routing_iter,
                name=str('item-item-coupling-' + str(i)),
                lam=self.capsule_lambda
            )
            self.item_item_coupling.append(item_item_capsule)

            user_item_capsule = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=self.input_length,
                num_caps_j=self.user_item_capsule_num,
                in_vec_len=self.numFactor,
                out_vec_len=self.numFactor,
                user_vec_len=self.numFactor,
                user_bias=False,
                T=self.dynamic_routing_iter,
                name=str('user-item-coupling-' + str(i)),
                lam=self.capsule_lambda,
                routing_type=self.routing_type
            )
            self.user_item_coupling.append(user_item_capsule)

            preference_capsule = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=1 + self.user_item_capsule_num + self.item_item_capsule_num,
                num_caps_j=self.preference_capsule_num,
                in_vec_len=self.numFactor,
                out_vec_len=self.numFactor,
                user_vec_len=self.numFactor,
                user_bias=False,
                T=self.dynamic_routing_iter,
                name=str('preference-coupling-' + str(i)),
                lam=self.capsule_lambda
            )
            self.preference_coupling.append(preference_capsule)

            self.config = config
            self.saver = tf.train.Saver()
            if config['load_model']:
                self.loadWeight()

            self.print_len = 1

            self.prints = {}
            self.prints_eval = {}

            self.prints['user_id_print'] = tf.Print(self.u_id, [self.u_id], message="u_id:", summarize=self.print_len)
            self.prints_eval['user_id_print'] = None
            self.prints['user_matrix_print'] = None
            self.prints_eval['user_matrix_print'] = None
            self.prints['input_seq_print'] = tf.Print(self.input_seq, [self.input_seq], message="input_seq:", summarize=self.print_len * self.input_length)
            self.prints_eval['input_seq_print'] = None
            self.prints['target_item_pos_print'] = tf.Print(self.target_seq_pos, [self.target_seq_pos], message="target_seq_pos:", summarize=self.print_len * self.target_length)
            self.prints_eval['target_item_pos_print'] = None
            self.prints['pos_matrix_print'] = None
            self.prints_eval['pos_matrix_print'] = None
            self.prints['target_item_neg_print'] = tf.Print(self.target_seq_neg, [self.target_seq_neg], message="target_seq_neg:", summarize=self.print_len * self.target_length)
            self.prints_eval['target_item_neg_print'] = None
            self.prints['neg_matrix_print'] = None
            self.prints_eval['neg_matrix_print'] = None
            self.prints['user_item_outputs_print'] = [n for n in range(self.numFactor)]
            self.prints_eval['user_item_outputs_print'] = [n for n in range(self.numFactor)]
            self.prints['item_item_outputs_print'] = [n for n in range(self.numFactor)]
            self.prints_eval['item_item_outputs_print'] = [n for n in range(self.numFactor)]
            self.prints['user_item_pos_print'] = [n for n in range(self.numFactor)]
            self.prints_eval['user_item_pos_print'] = [n for n in range(self.numFactor)]
            self.prints['item_item_pos_print'] = [n for n in range(self.numFactor)]
            self.prints_eval['item_item_pos_print'] = [n for n in range(self.numFactor)]
            self.prints['user_item_neg_print'] = [n for n in range(self.numFactor)]
            self.prints_eval['user_item_neg_print'] = [n for n in range(self.numFactor)]
            self.prints['item_item_neg_print'] = [n for n in range(self.numFactor)]
            self.prints_eval['item_item_neg_print'] = [n for n in range(self.numFactor)]

    def buildModel(self):
        with tf.variable_scope(tf.get_variable_scope()) as scope:

            userEmedding = tf.reshape(tf.nn.embedding_lookup(self.userEmbedding, self.u_id), [-1, self.numFactor])
            user_matrix = self.out_product(userEmedding, userEmedding)

            self.prints['user_matrix_print'] = tf.Print(user_matrix, [user_matrix],
                                                                                 message="user_matrix:",
                                                                                 summarize=self.numFactor * self.numFactor * self.print_len)
            user_matrix = tf.expand_dims(user_matrix, 1)

            item_matrices = []

            split_list = [1] * self.input_length
            itemIds = tf.split(self.input_seq, split_list, 1)

            for itemId in itemIds:
                item_embedding = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, itemId), [-1, self.numFactor])
                item_matrix = self.out_product(item_embedding, item_embedding)
                # [bs, numFactor, numFactor]
                item_matrix = tf.expand_dims(item_matrix, 1)
                item_matrices.append(item_matrix)
                # [bs, 1, numFactor, numFactor] * seq_len

            item_item_inputs = tf.concat(item_matrices, axis=1)
            # [bs, seq_len numFactor, numFactor]
            user_item_inputs = tf.concat(item_matrices, axis=1)

            matrix_split_list = [1] * self.numFactor
            item_item_inputs = tf.split(item_item_inputs, matrix_split_list, 3)
            # [bs, seq_len, numFactor, 1] * numFactor
            user_item_inputs = tf.split(user_item_inputs, matrix_split_list, 3)
            # [bs, seq_len+1, numFactor, 1] * numFactor

            user_matrix_slices = tf.split(user_matrix, matrix_split_list, 3)
            # [bs, 1, numFactor, 1] * numFactor

            preference_outputs = []
            for capsule_idx in range(self.numFactor):
                user_item_capsule = self.user_item_coupling[capsule_idx]
                # each capsule input : [bs, seq_len, numFactor, 1] -> [bs, num_capsule, capsule_len, 1]
                pos_user_item_output = user_item_capsule.get_output(user_item_inputs[capsule_idx], userEmedding)
                # [bs, num_j_capsule=num_capsule, vec_len=numFactor, 1]

                item_item_capsule = self.item_item_coupling[capsule_idx]
                pos_item_item_output = item_item_capsule.get_output(item_item_inputs[capsule_idx], None)
                # [bs, num_capsule, numFactor, 1]

                user_matrix_slice = user_matrix_slices[capsule_idx]
                # [bs, 1, numFactor, 1]

                preference_input = tf.concat([pos_user_item_output, pos_item_item_output, user_matrix_slice], 1)
                # [bs, 1 + num_capsule*2, numFactor, 1]

                preference_capsule = self.preference_coupling[capsule_idx]
                preference_output = preference_capsule.get_output(preference_input, None)
                # [bs, num_capsule, numFactor, 1]

                pos_user_item_output = tf.squeeze(pos_user_item_output, [3])
                pos_item_item_output = tf.squeeze(pos_item_item_output, [3])
                self.prints['user_item_outputs_print'][capsule_idx] = tf.Print(pos_user_item_output,
                                                                                        [pos_user_item_output],
                                                                                        message="user_item_output_%d:" % (
                                                                                            capsule_idx),
                                                                                        summarize=self.user_item_capsule_num * self.numFactor * self.print_len)

                self.prints['item_item_outputs_print'][capsule_idx] = tf.Print(pos_item_item_output,
                                                                                        [pos_item_item_output],
                                                                                        message="item_item_output_%d:" % (
                                                                                            capsule_idx),
                                                                                        summarize=self.item_item_capsule_num * self.numFactor * self.print_len)

                preference_outputs.append(preference_output)

            preference_matrices = tf.concat(preference_outputs, 3)
            # [bs, num_capsule, numFactor, numFactor]
            preference_embed = tf.reduce_sum(preference_matrices, axis=3, keepdims=False)
            # [bs, num_capsule, numFactor]
            merged_embed = tf.reduce_sum(preference_embed, axis=1, keepdims=False)
            # [bs, numFactor]

            pos_preds = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.target_seq_pos,
                tar_length=self.target_length,
            )
            neg_preds = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.target_seq_neg,
                tar_length=self.target_length
            )

            bpr_loss = - tf.reduce_sum(tf.log(tf.sigmoid(pos_preds - neg_preds)))
            l2_loss = self.factor_lambda * (tf.nn.l2_loss(self.userEmbedding) + tf.nn.l2_loss(self.itemEmbedding))

            self.cost = bpr_loss + l2_loss

            self.r_pred = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.pred_seq,
                tar_length=100
            )

            # target_item_embed = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, self.target_seq_pos), [-1, self.numFactor])
            # target_item_matrix = self.out_product(target_item_embed, target_item_embed)
            # self.prints['pos_matrix_print'] = tf.Print(target_item_matrix, [target_item_matrix],
            #                                                                      message="target_item_matrix:",
            #                                                                      summarize=self.numFactor * self.numFactor * self.print_len)
            # target_item_matrix_expand = tf.expand_dims(target_item_matrix, 1) # [bs, 1, numFactor, numFactor]
            # target_item_for_user_item_capsule = tf.concat([target_item_matrix_expand] * (self.input_length + 1), axis=1)
            # # [bs, seq_len + 1, numFactor, numFactor]
            # target_item_for_item_item_capsule = tf.concat([target_item_matrix_expand] * (self.input_length), axis=1)
            # # [bs, seq_len, numFactor, numFactor]
            # pos_inputs_for_user_item_capsule = tf.split(target_item_for_user_item_capsule, matrix_split_list, 3)
            # pos_inputs_for_item_item_capsule = tf.split(target_item_for_item_item_capsule, matrix_split_list, 3)
            #
            # neg_item_embed = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, self.target_seq_neg), [-1, self.numFactor])
            # neg_item_matrix = self.out_product(neg_item_embed, neg_item_embed)
            # self.prints['neg_matrix_print'] = tf.Print(neg_item_matrix, [neg_item_matrix],
            #                                                                      message="neg_item_matrix:",
            #                                                                      summarize=self.numFactor * self.numFactor * self.print_len)
            # neg_item_matrix_expand = tf.expand_dims(neg_item_matrix, 1)  # [bs, 1, numFactor, numFactor]
            # neg_item_for_user_item_capsule = tf.concat([neg_item_matrix_expand] * (self.input_length + 1), axis=1)
            # # [bs, seq_len + 1, numFactor, numFactor]
            # neg_item_for_item_item_capsule = tf.concat([neg_item_matrix_expand] * (self.input_length), axis=1)
            # # [bs, seq_len, numFactor, numFactor]
            # neg_inputs_for_user_item_capsule = tf.split(neg_item_for_user_item_capsule, matrix_split_list, 3)
            # neg_inputs_for_item_item_capsule = tf.split(neg_item_for_item_item_capsule, matrix_split_list, 3)
            #
            #
            # for capsule_idx in range(self.numFactor):
            #     user_item_capsule = self.user_item_coupling[capsule_idx]
            #     # each capsule input : [bs, seq_len, numFactor, 1] -> [bs, num_capsule, capsule_len, 1]
            #     pos_user_item_output = tf.squeeze(user_item_capsule.get_output(pos_inputs_for_user_item_capsule[capsule_idx], None), [3])
            #     # [bs, num_j_capsule=num_capsule, vec_len=numFactor, 1]
            #     self.prints['user_item_pos_print'][capsule_idx] = tf.Print(pos_user_item_output, [pos_user_item_output],
            #                                                                      message="user_item_pos_output_%d:" % (capsule_idx),
            #                                                                      summarize=self.user_item_capsule_num * self.numFactor * 3)
            #
            #
            #     item_item_capsule = self.item_item_coupling[capsule_idx]
            #     pos_item_item_output = tf.squeeze(item_item_capsule.get_output(pos_inputs_for_item_item_capsule[capsule_idx], None), [3])
            #     # [bs, num_capsule, numFactor, 1]
            #     self.prints['item_item_pos_print'][capsule_idx] = tf.Print(pos_item_item_output, [pos_item_item_output],
            #                                                                      message="item_item_pos_output_%d:" % (capsule_idx),
            #                                                                      summarize=self.item_item_capsule_num * self.numFactor * 3)
            #
            #     neg_user_item_output = tf.squeeze(user_item_capsule.get_output(neg_inputs_for_user_item_capsule[capsule_idx], None), [3])
            #     # [bs, num_j_capsule=num_capsule, vec_len=numFactor, 1]
            #     self.prints['user_item_neg_print'][capsule_idx] = tf.Print(neg_user_item_output,
            #                                                                      [neg_user_item_output],
            #                                                                      message="user_item_neg_output_%d:" % (
            #                                                                          capsule_idx),
            #                                                                      summarize=self.user_item_capsule_num * self.numFactor * 3)
            #
            #     neg_item_item_output = tf.squeeze(item_item_capsule.get_output(neg_inputs_for_item_item_capsule[capsule_idx], None), [3])
            #     # [bs, num_j_capsule=num_capsule, vec_len=numFactor, 1]
            #     self.prints['item_item_neg_print'][capsule_idx] = tf.Print(neg_item_item_output,
            #                                                                             [neg_item_item_output],
            #                                                                             message="item_item_neg_output_%d:" % (
            #                                                                                 capsule_idx),
            #                                                                             summarize=self.item_item_capsule_num * self.numFactor * 3)


    def getVisualData(self, seq_Id):
        # compute start and end
        start = time.time()
        neg_seq_batch = []
        reset_mat = np.ones([self.trainBatchSize, self.numFactor])

        userIdx = self.train_users[seq_Id]
        user_batch = [userIdx] * self.trainBatchSize

        # lazy operate
        cluster_batch = [1] * self.trainBatchSize
        input_seq_batch = self.train_sequences_input[seq_Id] * self.trainBatchSize
        pos_seq_batch = self.train_sequences_target[seq_Id] * self.trainBatchSize

        neg_items = []
        positiveItems = self.user_items_train[userIdx]

        while (len(neg_items) < self.trainBatchSize):
            negativeItemIdx = random.randint(0, self.numItem - 1)
            while (negativeItemIdx in positiveItems) or (negativeItemIdx in neg_items):
                negativeItemIdx = random.randint(0, self.numItem - 1)
            neg_items.append(negativeItemIdx)

        for i in range(self.target_length):
            user_batch = np.array(user_batch).reshape((self.trainBatchSize, 1))
            cluster_batch = np.array(cluster_batch).reshape((self.trainBatchSize, 1))
            input_seq_batch = np.array(input_seq_batch).reshape((self.trainBatchSize, self.input_length))
            pos_seq_batch = np.array(pos_seq_batch).reshape((self.trainBatchSize, 1))
            neg_seq_batch = np.array(neg_items).reshape((self.trainBatchSize, 1))

        end = time.time()
        # self.logger.info("time of collect a batch of data: " + str((end - start)) + " seconds")
        # self.logger.info("batch Id: " + str(batchId))
        feed_dict = {
            self.u_id: user_batch,
            self.cluster_id: cluster_batch,
            self.input_seq: input_seq_batch,
            self.target_seq_pos: pos_seq_batch,
            self.target_seq_neg: neg_seq_batch,
            self.dropout_keep_placeholder: self.dropout_keep,
            self.is_train: True,
        }

        return feed_dict

    def load_functions(self):
        self.evaluateRanking(1, 1)
        self.visualization()


    def visualization(self):
        # collect variables
        for seq_id in [5000]:
            feed_dict = self.getVisualData(seq_id)
            for key, tensors_to_print in self.prints.items():
                if isinstance(tensors_to_print, (list,)):
                    for idx in range(len(tensors_to_print)):
                        tensor = tensors_to_print[idx]
                        self.prints_eval[key][idx] = tensor.eval(feed_dict=feed_dict)
                else:
                    self.prints_eval[key] = tensors_to_print.eval(feed_dict=feed_dict)

            # visualization
            # user_item_visualization
            embeds = []
            for dim_idx in range(self.numFactor):
                for capsule_idx in range(self.user_item_capsule_num):
                    # [bs, num_capsule, numFactor, 1]
                    user_item = self.prints_eval['user_item_outputs_print'][dim_idx][0][capsule_idx]
                    user_item_pos = self.prints_eval['user_item_pos_print'][dim_idx][0][capsule_idx]
                    embeds.append(user_item)
                    embeds.append(user_item_pos)

                    for batch_idx in range(self.trainBatchSize):
                        neg_item_embeding = self.prints_eval['user_item_neg_print'][dim_idx][batch_idx][capsule_idx]
                        embeds.append(neg_item_embeding)

                    user_item_visualize = PCACapsule(embeds, 3, self.config, seq_id)
                    user_item_visualize.visualization(
                        name='user-item-dim' + str(dim_idx) + '-capsule-' + str(capsule_idx))
                    embeds = []

            # # item_item_visualization
            # embeds = []
            # for dim_idx in range(self.numFactor):
            #     for capsule_idx in range(self.item_item_capsule_num):
            #         # [bs, num_capsule, numFactor, 1]
            #         item_item = self.prints_eval['item_item_outputs_print'][dim_idx][0][capsule_idx]
            #         item_item_pos = self.prints_eval['item_item_pos_print'][dim_idx][0][capsule_idx]
            #         embeds.append(item_item)
            #         embeds.append(item_item_pos)
            #
            #         for batch_idx in range(self.trainBatchSize):
            #             neg_item_embeding = self.prints_eval['item_item_neg_print'][dim_idx][batch_idx][capsule_idx]
            #             embeds.append(neg_item_embeding)
            #
            #         item_item_visualize = PCACapsule(embeds, 3, self.config, seq_id)
            #         item_item_visualize.visualization(
            #             name='item-item-dim' + str(dim_idx) + '-capsule-' + str(capsule_idx), type='pca')
            #         embeds = []

    def saveWeight(self):
        save_dir = './save_model/' + self.config['fileName'] + '-' + self.name
        if os.path.isdir(save_dir) is False:
            os.makedirs(save_dir)
        save_path = self.saver.save(self.sess, save_dir + '/' + self.name + '.ckpt')
        print("Model saved in path: %s" % save_path)

    def loadWeight(self):
        load_dir = './save_model/' + self.config['fileName'] + '-' + self.name
        self.saver.restore(self.sess, load_dir + '/' + self.name + '.ckpt')
        print("Model restored.")
