import tensorflow as tf
from recommender.CapSeq_Basic import CapSeqBasic
from component.MLP import MLP


class MUIC_Recommender(CapSeqBasic):

    def __init__(self, dataModel, config):

        super(MUIC_Recommender, self).__init__(dataModel, config)

        self.name = 'MCN-mlp'

        self.fc_dim = [self.numFactor * self.numFactor * (1 + self.input_length)] + config['fc_dim']
        self.MLP = MLP(self.fc_dim, dropout_keep=self.dropout_keep_placeholder, name='replace-capsule-')

        self.last_weight = tf.get_variable(name="last-mlp-weight", shape=[self.fc_dim[-1], self.numFactor], dtype=tf.float32,
                                           initializer=tf.contrib.layers.xavier_initializer(),
                                           )


    def buildModel(self):
        with tf.variable_scope(tf.get_variable_scope()) as scope:

            userEmedding = tf.reshape(tf.nn.embedding_lookup(self.userEmbedding, self.u_id), [-1, self.numFactor])
            user_matrix = self.out_product(userEmedding, userEmedding)
            user_matrix_flatten = tf.reshape(user_matrix, [-1, self.numFactor * self.numFactor])

            flatten_matrices = []
            flatten_matrices.append(user_matrix_flatten)

            split_list = [1] * self.input_length
            itemIds = tf.split(self.input_seq, split_list, 1)

            for itemId in itemIds:
                item_embedding = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, itemId), [-1, self.numFactor])
                item_matrix = self.out_product(item_embedding, item_embedding)
                item_matrix_flatten = tf.reshape(item_matrix, [-1, self.numFactor * self.numFactor])
                flatten_matrices.append(item_matrix_flatten)
                # [bs, 1, numFactor * numFactor] * seq_len

            mlp_output = self.MLP.get_output(feature_input=tf.concat(flatten_matrices, axis=1))

            merged_embed = tf.matmul(mlp_output, self.last_weight)

            pos_preds = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.target_seq_pos,
                tar_length=self.target_length,
            )
            neg_preds = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.target_seq_neg,
                tar_length=self.target_length
            )

            bpr_loss = - tf.reduce_sum(tf.log(tf.sigmoid(pos_preds - neg_preds)))
            l2_loss = self.factor_lambda * (tf.nn.l2_loss(self.userEmbedding) + tf.nn.l2_loss(self.itemEmbedding))

            self.cost = bpr_loss + l2_loss

            self.r_pred = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.pred_seq,
                tar_length=self.eval_item_num
            )
