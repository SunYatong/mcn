import tensorflow as tf
from recommender.CapSeq_Basic import CapSeqBasic
from component.Capsule import Capsule_Component
import os
from component.Attention import Attention


class MUIC_Recommender(CapSeqBasic):

    def __init__(self, dataModel, config):

        super(MUIC_Recommender, self).__init__(dataModel, config)

        self.name = 'MCN-atten'
        self.user_item_capsule_num = config['user_item_capsule_num']
        self.item_item_capsule_num = config['item_item_capsule_num']
        self.preference_capsule_num = config['preference_capsule_num']
        self.capsule_lambda = config['capsule_lambda']

        # Conv-Capsule Layers
        self.hor_pattern_capsules = []
        self.user_embeddings = []
        self.item_item_coupling = []
        self.user_item_coupling = []
        self.preference_coupling = []

        self.self_atten_network = Attention()

        self.config = config
        self.saver = tf.train.Saver()
        if config['load_model']:
            self.loadWeight()

    def buildModel(self):
        with tf.variable_scope(tf.get_variable_scope()) as scope:

            userEmedding = tf.reshape(tf.nn.embedding_lookup(self.userEmbedding, self.u_id), [-1, self.numFactor])
            user_matrix = self.out_product(userEmedding, userEmedding)
            user_matrix = tf.expand_dims(user_matrix, 1)

            item_matrices = []

            split_list = [1] * self.input_length
            itemIds = tf.split(self.input_seq, split_list, 1)

            for itemId in itemIds:
                item_embedding = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, itemId), [-1, self.numFactor])
                item_matrix = self.out_product(item_embedding, item_embedding)
                item_matrix = tf.expand_dims(item_matrix, 1)
                # [bs, 1, numFactor, numFactor]
                item_matrices.append(item_matrix)

            item_item_inputs = tf.concat(item_matrices, axis=1)
            # [bs, seq_len numFactor, numFactor]
            user_item_inputs = tf.concat(item_matrices + [user_matrix], axis=1)
            # [bs, seq_len+1 numFactor, numFactor]
            matrix_split_list = [1] * self.numFactor
            item_item_inputs = tf.split(item_item_inputs, matrix_split_list, 3)
            # [bs, seq_len, numFactor] * numFactor
            user_item_inputs = tf.split(user_item_inputs, matrix_split_list, 3)
            # [bs, seq_len+1, numFactor] * numFactor
            user_matrix_slices = tf.split(user_matrix, matrix_split_list, 3)
            # [bs, 1, numFactor, 1] * numFactor

            preference_outputs = []
            for capsule_idx in range(self.numFactor):
                user_item_inputs[capsule_idx] = tf.squeeze(user_item_inputs[capsule_idx], 3)
                item_item_inputs[capsule_idx] = tf.squeeze(item_item_inputs[capsule_idx], 3)
                user_item_output = self.self_atten_network.multihead_attention(queries=user_item_inputs[capsule_idx], keys=user_item_inputs[capsule_idx])
                # [bs, seq_len+1, numFactor]
                user_item_output = tf.reduce_sum(user_item_output, axis=1, keep_dims=True)
                # [bs, 1, numFactor]

                item_item_output = self.self_atten_network.multihead_attention(queries=item_item_inputs[capsule_idx], keys=item_item_inputs[capsule_idx])
                item_item_output = tf.reduce_sum(item_item_output, axis=1, keep_dims=True)
                # [bs, 1, numFactor]

                user_matrix_slice = tf.squeeze(user_matrix_slices[capsule_idx], 3)
                # [bs, 1, numFactor]

                preference_input = tf.concat([user_item_output, item_item_output, user_matrix_slice], 1)
                # [bs, 3, numFactor]

                preference_output = self.self_atten_network.multihead_attention(queries=preference_input, keys=preference_input)
                # [bs, 3, numFactor]
                preference_output = tf.reduce_sum(preference_output, axis=1, keep_dims=True)
                # [bs, 1, numFactor]
                preference_outputs.append(preference_output)

            preference_matrices = tf.concat(preference_outputs, 1)
            # [bs, num_capsule, numFactor]
            merged_embed = tf.reduce_sum(preference_matrices, axis=1, keepdims=False)
            # [bs, numFactor]

            pos_preds = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.target_seq_pos,
                tar_length=self.target_length,
            )
            neg_preds = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.target_seq_neg,
                tar_length=self.target_length
            )

            bpr_loss = - tf.reduce_sum(tf.log(tf.sigmoid(pos_preds - neg_preds)))
            l2_loss = self.factor_lambda * (tf.nn.l2_loss(self.userEmbedding) + tf.nn.l2_loss(self.itemEmbedding))

            self.cost = bpr_loss + l2_loss

            self.r_pred = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.pred_seq,
                tar_length=100
            )

    def saveWeight(self):
        save_dir = './save_model/' + self.config['fileName'] + '-' + self.name
        if os.path.isdir(save_dir) is False:
            os.makedirs(save_dir)
        save_path = self.saver.save(self.sess, save_dir + '/' + self.name + '.ckpt')
        print("Model saved in path: %s" % save_path)

    def loadWeight(self):
        load_dir = './save_model/' + self.config['fileName'] + '-' + self.name
        self.saver.restore(self.sess, load_dir + '/' + self.name + '.ckpt')
        print("Model restored.")
