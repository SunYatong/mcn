from src.recommender.BasicRcommender import BasicRecommender

class ExternalModel(BasicRecommender):

    def __init__(self, dataModel, config):
        super(ExternalModel, self).__init__(dataModel, config)
        self.inputPath = './HFT_ranking/' + self.fileName + '/recommend'
        self.user_rec_list = {}

    def buildModel(self):
        with open(self.inputPath) as inputFile:
            for line in inputFile:
                userId, itemIds = line.strip().split(':')
                userIdx = self.userIdToUserIdx[userId]
                rec_list = []
                for itemId in itemIds.split(','):
                    if itemId == '':
                        pass
                    else:
                        itemIdx = self.itemIdToItemIdx[itemId]
                        rec_list.append(itemIdx)
                self.user_rec_list[userIdx] = rec_list

    def trainModel(self):
        self.evaluateRanking(0, 0)

    def getPredList_ByUserIdxList(self, user_idices):
        outputLists = []
        for i in range(len(user_idices)):
            userIdx = user_idices[i]
            outputLists.append(self.user_rec_list[userIdx])

        return outputLists, 0.0, 0.0, 0.0
