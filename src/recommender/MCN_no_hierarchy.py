import tensorflow as tf
from recommender.CapSeq_Basic import CapSeqBasic
from component.Capsule import Capsule_Component
import os


class MUIC_Recommender(CapSeqBasic):

    def __init__(self, dataModel, config):

        super(MUIC_Recommender, self).__init__(dataModel, config)

        self.name = 'MCN-no-hiererchy'
        self.item_item_capsule_num = config['item_item_capsule_num']
        self.capsule_lambda = config['capsule_lambda']

        # Conv-Capsule Layers
        self.hor_pattern_capsules = []
        self.user_embeddings = []
        self.item_item_coupling = []
        for i in range(self.numFactor):
            item_item_capsule = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=self.input_length,
                num_caps_j=self.item_item_capsule_num,
                in_vec_len=self.numFactor,
                out_vec_len=self.numFactor,
                user_vec_len=self.numFactor,
                user_bias=False,
                T=self.dynamic_routing_iter,
                name=str('item-item-coupling-' + str(i)),
                lam=self.capsule_lambda
            )
            self.item_item_coupling.append(item_item_capsule)

            self.config = config
            self.saver = tf.train.Saver()
            if config['load_model']:
                self.loadWeight()

    def buildModel(self):
        with tf.variable_scope(tf.get_variable_scope()) as scope:

            userEmedding = tf.reshape(tf.nn.embedding_lookup(self.userEmbedding, self.u_id), [-1, self.numFactor])

            item_matrices = []

            split_list = [1] * self.input_length
            itemIds = tf.split(self.input_seq, split_list, 1)

            for itemId in itemIds:
                item_embedding = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, itemId), [-1, self.numFactor])
                item_matrix = self.out_product(userEmedding, item_embedding)
                # [bs, numFactor, numFactor]
                item_matrix = tf.expand_dims(item_matrix, 1)
                item_matrices.append(item_matrix)
                # [bs, 1, numFactor, numFactor] * seq_len

            item_item_inputs = tf.concat(item_matrices, axis=1)
            # [bs, seq_len numFactor, numFactor]


            matrix_split_list = [1] * self.numFactor
            item_item_inputs = tf.split(item_item_inputs, matrix_split_list, 3)
            # [bs, seq_len, numFactor, 1] * numFactor

            preference_outputs = []
            for capsule_idx in range(self.numFactor):

                item_item_capsule = self.item_item_coupling[capsule_idx]
                item_item_output = item_item_capsule.get_output(item_item_inputs[capsule_idx], None)
                # [bs, num_capsule, numFactor, 1]

                preference_outputs.append(item_item_output)

            preference_matrices = tf.concat(preference_outputs, 3)
            # [bs, num_capsule, numFactor, numFactor]
            preference_embed = tf.reduce_sum(preference_matrices, axis=3, keepdims=False)
            # [bs, num_capsule, numFactor]
            merged_embed = tf.reduce_sum(preference_embed, axis=1, keepdims=False)
            # [bs, numFactor]

            pos_preds = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.target_seq_pos,
                tar_length=self.target_length,
            )
            neg_preds = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.target_seq_neg,
                tar_length=self.target_length
            )

            bpr_loss = - tf.reduce_sum(tf.log(tf.sigmoid(pos_preds - neg_preds)))
            l2_loss = self.factor_lambda * (tf.nn.l2_loss(self.userEmbedding) + tf.nn.l2_loss(self.itemEmbedding))

            self.cost = bpr_loss + l2_loss

            self.r_pred = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.pred_seq,
                tar_length=100
            )

    def saveWeight(self):
        save_dir = './save_model/' + self.config['fileName'] + '-' + self.name
        if os.path.isdir(save_dir) is False:
            os.makedirs(save_dir)
        save_path = self.saver.save(self.sess, save_dir + '/' + self.name + '.ckpt')
        print("Model saved in path: %s" % save_path)

    def loadWeight(self):
        load_dir = './save_model/' + self.config['fileName'] + '-' + self.name
        self.saver.restore(self.sess, load_dir + '/' + self.name + '.ckpt')
        print("Model restored.")
