import tensorflow as tf
from recommender.CapSeq_Basic import CapSeqBasic
from component.Capsule import Capsule_Component
import os


class MUIC_Recommender(CapSeqBasic):

    def __init__(self, dataModel, config):

        super(MUIC_Recommender, self).__init__(dataModel, config)

        self.name = 'MCN'
        self.user_item_capsule_num = config['user_item_capsule_num']
        self.item_item_capsule_num = config['item_item_capsule_num']
        self.preference_capsule_num = config['preference_capsule_num']
        self.capsule_lambda = config['capsule_lambda']

        # Conv-Capsule Layers
        self.hor_pattern_capsules = []
        self.user_embeddings = []
        self.item_item_coupling = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=self.input_length,
                num_caps_j=self.item_item_capsule_num,
                in_vec_len=self.numFactor,
                out_vec_len=self.numFactor,
                user_vec_len=self.numFactor,
                user_bias=False,
                T=self.dynamic_routing_iter,
                name=str('item-item-coupling-'),
                lam=self.capsule_lambda
            )
        self.user_item_coupling = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=1 + self.input_length,
                num_caps_j=self.user_item_capsule_num,
                in_vec_len=self.numFactor,
                out_vec_len=self.numFactor,
                user_vec_len=self.numFactor,
                user_bias=False,
                T=self.dynamic_routing_iter,
                name=str('user-item-coupling-'),
                lam=self.capsule_lambda
            )
        self.preference_coupling = Capsule_Component(
                bs=self.trainBatchSize,
                num_caps_i=1+self.user_item_capsule_num+self.item_item_capsule_num,
                num_caps_j=self.preference_capsule_num,
                in_vec_len=self.numFactor,
                out_vec_len=self.numFactor,
                user_vec_len=self.numFactor,
                user_bias=False,
                T=self.dynamic_routing_iter,
                name=str('preference-coupling-'),
                lam=self.capsule_lambda
            )

        self.config = config
        self.saver = tf.train.Saver()
        if config['load_model']:
            self.loadWeight()

    def buildModel(self):
        with tf.variable_scope(tf.get_variable_scope()) as scope:

            userEmedding = tf.reshape(tf.nn.embedding_lookup(self.userEmbedding, self.u_id), [-1, 1, self.numFactor, 1])

            split_list = [1] * self.input_length
            itemIds = tf.split(self.input_seq, split_list, 1)

            user_item_input_list = []
            item_item_input_list = []
            for itemId in itemIds:
                item_embedding = tf.reshape(tf.nn.embedding_lookup(self.itemEmbedding, itemId), [-1, 1, self.numFactor, 1])
                user_item_input_list.append(item_embedding)
                item_item_input_list.append(item_embedding)
            user_item_input_list.append(userEmedding)

            item_item_inputs = tf.concat(item_item_input_list, axis=1)
            # [bs, seq_len numFactor, 1]

            user_item_inputs = tf.concat(user_item_input_list, axis=1)

            user_item_capsule = self.user_item_coupling
            # each capsule input : [bs, seq_len, numFactor, 1] -> [bs, num_capsule, capsule_len, 1]
            user_item_output = user_item_capsule.get_output(user_item_inputs, None)
            # [bs, num_j_capsule=num_capsule, vec_len=numFactor, 1]

            item_item_capsule = self.item_item_coupling
            item_item_output = item_item_capsule.get_output(item_item_inputs, None)
            # [bs, num_capsule, numFactor, 1]

            preference_input = tf.concat([user_item_output, item_item_output, userEmedding], 1)
            # [bs, 1 + num_capsule*2, numFactor, 1]

            preference_capsule = self.preference_coupling
            preference_output = preference_capsule.get_output(preference_input, None)
            # [bs, num_capsule, numFactor, 1]

            preference_embed = tf.reduce_sum(preference_output, axis=3, keepdims=False)
            # [bs, num_capsule, numFactor]
            merged_embed = tf.reduce_sum(preference_embed, axis=1, keepdims=False)
            # [bs, numFactor]

            pos_preds = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.target_seq_pos,
                tar_length=self.target_length,
            )
            neg_preds = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.target_seq_neg,
                tar_length=self.target_length
            )

            bpr_loss = - tf.reduce_sum(tf.log(tf.sigmoid(pos_preds - neg_preds)))
            l2_loss = self.factor_lambda * (tf.nn.l2_loss(self.userEmbedding) + tf.nn.l2_loss(self.itemEmbedding))

            self.cost = bpr_loss + l2_loss

            self.r_pred = self.pred_for_a_user(
                W=self.itemEmbedding,
                b=self.itemBias,
                numFactor=self.numFactor,
                input_feature=merged_embed,
                ids=self.pred_seq,
                tar_length=100
            )

    def saveWeight(self):
        save_dir = './save_model/' + self.config['fileName'] + '-' + self.name
        if os.path.isdir(save_dir) is False:
            os.makedirs(save_dir)
        save_path = self.saver.save(self.sess, save_dir + '/' + self.name + '.ckpt')
        print("Model saved in path: %s" % save_path)

    def loadWeight(self):
        load_dir = './save_model/' + self.config['fileName'] + '-' + self.name
        self.saver.restore(self.sess, load_dir + '/' + self.name + '.ckpt')
        print("Model restored.")
