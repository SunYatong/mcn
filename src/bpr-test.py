
from src.recommender.BPR import BPRRecommender
from src.data_model.SequenceDataModel import SequenceDataModel


if __name__ == '__main__':

    # instant_video_5_core, digital_music_5_core_100, baby_5_core_100  apps_for_android_5 video_games_5

    config = {
        'generate_seq': True,
        'splitterType': 'userTimeRatio',
        'fileName': 'ml-100k',
        'trainType': 'test',
        'threshold': 0,
        'learnRate': 0.001,
        'maxIter': 1000,
        'trainBatchSize': 939,
        'testBatchSize': 939,
        'numFactor': 32,
        'topN': 10,
        'factor_lambda': 0.01,
        'goal': 'ranking',
        'verbose': False,
        'input_length': 3,
        'target_length': 1,
        'dropout_keep': 0.8,
        'capsule_num': 1,
        'eval_item_num': 100,
        'early_stop': True,
        'mlp_lam': 0.001,
        'save_model': True,
        'random_seed': 1,
    }

    for fileName in ['ml-100k']:
        config['fileName'] = fileName
        dataModel = SequenceDataModel(config)
        dataModel.buildModel()

        for learnRate in [0.0001]:
            for factor_lambda in [0.001]:
                recommender = BPRRecommender(dataModel, config)
                recommender.run()





