
from recommender.NCF import NCFRecommender
from data_model.SequenceDataModel import SequenceDataModel

if __name__ == '__main__':

    # instant_video_5_core, digital_music_5_core_100, baby_5_core_100  apps_for_android_5 video_games_5

    config = {
        'test-info': 'test',
        'item-weight-more': False,
        'generate_seq': True,
        'splitterType': 'userTimeRatio',
        'fileName': 'ml-100k',
        'trainType': 'test',
        'threshold': 0,
        'learnRate': 0.001,
        'maxIter': 1000,
        'trainBatchSize': 830,
        'testBatchSize': 830,
        'numFactor': 16,
        'topN': 10,
        'factor_lambda': 0.1,
        'goal': 'ranking',
        'verbose': False,

        'neg_sample_num': 5,

        'dropout_keep': 0.5,
        'fc_dim': [32, 16],
        'pred_mlp_dim': [32, 8],
        'eval_item_num': 100,
        'early_stop': True,
        'random_seed': 1,
        'save_model': False,
        'load_model': False,
    }

    for fileName in ['electronics-seq']:
        config['fileName'] = fileName

        dataModel = SequenceDataModel(config)
        dataModel.buildModel()
        for neg_sample_num in [5, 10, 20]:
            for drop_keep in [1.0, 0.8, 0.6, 0.4, 0.2]:
                for fc_dim_2 in [16, 8]:
                    for pred_mlp_2 in [32, 16, 8]:
                        fc_dim_1 = 32
                        pred_mlp_1 = fc_dim_2 + 16
                        if fc_dim_2 < fc_dim_1 and pred_mlp_2 < fc_dim_1:
                            config['neg_sample_num'] = neg_sample_num
                            config['dropout_keep'] = drop_keep
                            config['fc_dim'] = [fc_dim_1, fc_dim_2]
                            config['pred_mlp_dim'] = [pred_mlp_1, pred_mlp_2]
                            recommender = NCFRecommender(dataModel, config)
                            recommender.run()























