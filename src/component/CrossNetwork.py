import tensorflow as tf

class CrossNetwork(object):

    def __init__(self):
        pass

    # def cross_variable_creat(self, column_num):
    #     # print ('in cross_variable_creat, column_num:', column_num, 'type:', type(column_num))
    #     w = tf.Variable(tf.random_normal((column_num, 1), mean=0.0, stddev=0.5), dtype=tf.float32)
    #     b = tf.Variable(tf.random_normal((column_num, 1), mean=0.0, stddev=0.5), dtype=tf.float32)
    #     return w, b

    def cross_op(self, x0, x, column_num, layer_num):

        with tf.variable_scope('cross-network-cross-op-%s' % (layer_num), reuse=tf.AUTO_REUSE):
            w = tf.get_variable(
                name="cross_W",
                dtype=tf.float32,
                initializer=tf.random_normal((column_num, 1), mean=0.0, stddev=0.5)
            )

            b = tf.get_variable(
                name="corss_W",
                dtype=tf.float32,
                initializer=tf.random_normal((column_num, 1), mean=0.0, stddev=0.5)
            )
        # x0 and x, shape mxd ; # w  and b, shape dx1
        # print ('x0 in cross_op', x0.get_shape())
        # print ('x  in corss_op', x.get_shape())
        # print ('w  in cross_op', w.get_shape())
        # print ('b  in cross_op', b.get_shape())
        x0 = tf.expand_dims(x0, axis=2)  # mxdx1
        x = tf.expand_dims(x, axis=2)  # mxdx1
        # print ('x0 in cross_op, after expand_dims', x0.get_shape())
        # print ('x  in cross_op, after expand_dims', x.get_shape())
        multiple = w.get_shape().as_list()[0]

        x0_broad_horizon = tf.tile(x0, [1, 1, multiple])  # mxdx1 -> mxdxd #
        # print ('x0_broad in cross_op, after tf.tile', x0_broad_horizon.get_shape())
        x_broad_vertical = tf.transpose(tf.tile(x, [1, 1, multiple]), [0, 2, 1])  # mxdx1 -> mxdxd #
        # print ('x_broad_vertical, after tf.tile and tf.trans', x_broad_vertical.get_shape())
        w_broad_horizon = tf.tile(w, [1, multiple])  # dx1 -> dxd #
        # print ('w_broad_horizon, after tf.tile', w_broad_horizon.get_shape())
        mid_res = tf.multiply(tf.multiply(x0_broad_horizon, x_broad_vertical),
                              w)  # mxdxd # here use broadcast compute #
        # print ('mid_res, after multiply x0_broad, x_broad, w', mid_res.get_shape())
        res = tf.reduce_sum(mid_res, axis=2)  # mxd #
        # print ('res, after tf.reduce_sum(2)', res.get_shape())
        res = res + tf.transpose(b)  # mxd + 1xd # here also use broadcast compute #a
        # print ('res, after +', res.get_shape())
        # print ('-----------------------------------')
        return res

    def get_output(self, x0, layer_num, column_num):

        x_l = None
        for i in range(layer_num):
            if i == 0:
                x_l = x0
            x_l_plus_1 = self.cross_op(x0, x_l, column_num, layer_num=i) + x_l
            x_l = x_l_plus_1
        return x_l