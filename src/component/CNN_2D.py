import tensorflow as tf


class CNN_Pool_2d_Compoment:

    def __init__(self,
                 filter_num,
                 filter_heights,
                 filter_widths,
                 output_size,
                 cnn_lambda=0.01,
                 dropout_keep_prob=0.8,
                 name='2d-cnn-'
                 ):
        # dimension settings
        self.num_filters = filter_num
        self.filter_heights = filter_heights
        self.filter_widths = filter_widths
        self.output_size = output_size

        # regularization parameters
        self.cnn_lambda = cnn_lambda
        self.dropout_keep_prob = dropout_keep_prob

        self.name = name

        # cnn weights
        weights_regularizer = tf.contrib.layers.l2_regularizer(self.cnn_lambda)
        self.num_filters_total = self.num_filters * len(self.filter_heights)
        for i in range(len(self.filter_heights)):

            filter_height = self.filter_heights[i]
            filter_width = self.filter_widths[i]

            with tf.variable_scope(self.name + "-conv-maxpool-%s-%s-" % (filter_height, filter_width), reuse=None, regularizer=weights_regularizer):
                # Convolution Layer
                filter_shape = [filter_height, filter_width, 1, self.num_filters]
                W = tf.get_variable(name="W", initializer=tf.truncated_normal(filter_shape, stddev=0.1),
                                    dtype=tf.float32)
                b = tf.get_variable(name="b", initializer=tf.constant(0.1, shape=[self.num_filters]), dtype=tf.float32)

        with tf.variable_scope(self.name + "-cnn_final_output", reuse=None):
            output_W = tf.get_variable(
                "W",
                shape=[self.num_filters_total, self.output_size],
                initializer=tf.contrib.layers.xavier_initializer(),
                dtype=tf.float32
            )
            b = tf.get_variable(
                name="b",
                initializer=tf.constant(0.1, shape=[self.output_size]),
                dtype=tf.float32
            )

    def get_l2_loss(self):
        with tf.variable_scope("cnn_final_output", reuse=True):
            output_W = tf.get_variable(
                "W",
                shape=[self.num_filters_total, self.output_size],
                initializer=tf.contrib.layers.xavier_initializer(),
                dtype=tf.float32
            )
        return self.cnn_lambda * tf.nn.l2_loss(output_W)


    def get_conv_pool_output(self, input_image):

        '''

        :param input_image: [batch_size, height, width]
        :return: [batch_size, num_filter]
        '''

        review_input_expanded = tf.expand_dims(input_image, -1)
        input_height = int(input_image.get_shape()[1])
        input_width = int(input_image.get_shape()[2])

        pooled_outputs = []

        # Create a convolution + maxpool layer for each filter size
        for i in range(len(self.filter_heights)):

            filter_height = self.filter_heights[i]
            filter_width = self.filter_widths[i]

            with tf.variable_scope(self.name + "-conv-maxpool-%s-%s-" % (filter_height, filter_width), reuse=True):

                # Convolution Layer
                # filter_shape = [filter_height, filter_width, in_channels, out_channels]
                filter_shape = [filter_height, filter_width, 1, self.num_filters]
                W = tf.get_variable(name="W", initializer=tf.truncated_normal(filter_shape, stddev=0.1),
                                    dtype=tf.float32)
                b = tf.get_variable(name="b", initializer=tf.constant(0.1, shape=[self.num_filters]),
                                    dtype=tf.float32)
                # conv = [batchsize, self.maxReviewLength - filter_size + 1, in_channels, out_channels]
                conv = tf.nn.conv2d(
                    input=review_input_expanded,
                    filter=W,
                    strides=[1, 1, 1, 1],
                    padding="VALID",
                    name="conv_layer",
                    use_cudnn_on_gpu=True)
                # Apply nonlinearity
                h = tf.nn.relu(tf.nn.bias_add(conv, b), name="relu")
                # Maxpooling over the outputs
                # pooled = [batchsize, 1, in_channels, out_channels]
                pooled = tf.nn.max_pool(
                    value=h,
                    ksize=[1, input_height - filter_height + 1, input_width - filter_width + 1, 1],
                    strides=[1, 1, 1, 1],
                    padding='VALID',
                    name="pool_layer")
                pooled_outputs.append(pooled)

        # Combine all the pooled features
        h_pool = tf.concat(pooled_outputs, 3)
        h_pool_flat = tf.reshape(h_pool, [-1, self.num_filters_total])

        return h_pool_flat





