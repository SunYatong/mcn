



# Abstract
Abstract的一般写法：
第一句，强调当前所用内容（Reivew, tag, image）或技术(GAN, CNN, RNN)的重要性。
第二句，强调现有相关工作的主要缺陷是什么。
第三句，为解决这些问题，本文提出了什么模型（大致介绍）。
    然后，用1-2句话写main contribution 1, 
    用1-2句写main contribution 2，
    用1-2句写 main contribution 3 (一般是指实验）。最后用一句强调所提工作的意义（可选）。
    
第一句直入主题，第二句指出问题，其它都是讲自己提出的模型，具体的设计、实验表现、意义等。

# Introduction
第一段（研究的主题是什么，该主题面临的主要问题是多少）；
第二段（为解决该问题，前半段写现有工作的一般作法是什么，后半段写现有工作的主要不足是什么）；
第三段（本文的工作是什么，主要的contribution是什么）
该主题面临的主要问题是多少 --》 该主题面临的主要问题是什么

# Related Work
Related work的一般写法：将现有工作按某个维度进行分类，
维度可以是按时间（早期，最近，最相关的工作）、方法类型（memory-based, model-based；traditional, deep learning）、问题类型（rating prediction, item recommendation）等。
每个维度写成一段，写具体的工作是什么，存在什么问题，
可以单独写每个算法的问题，也可以写一类算法的问题。
Related Work的另一个重要因素是告诉读者，本文所提方法所处的文献位置是在哪里，属于哪一类，跟现有工作的重要区别和差异是什么。

# Our Model
- problem definition
- the whole model structure
- attention network
    

# Experiments
- Datasets (statistics and pre-process)
- Comparison Methods
- Experimental Results and Analysis
    -- Model Comparison
    -- Parameter Analysis(influences of lambda and dropout)
    -- Model Ablation


# Conclusion and Future Work

# References