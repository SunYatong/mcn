import recommender.ReviewBPR_CNN
import src.data_model.ReviewDataModel

if __name__ == '__main__':

    # digital_music_5_core_100, baby_5_core_100
    config = {
        'negative_sample': 0,
        'splitterType': 'userTimeRatio',
        'fileName': 'apps_for_android_5',
        'trainType': 'test',
        'threshold': 0,
        'wordVecSize': 200,
        'learnRate': 0.001,
        'maxIter': 300,
        'trainBatchSize': 1000,
        'testBatchSize': 100,
        'numFactor': 32,
        'filters_sizes': [1, 2],
        'num_filters': 5,
        'topN': 10,
        'factor_lambda': 0.1,
        'cnn_lambda': 0.1,
        'atten_lambda': 0.1,
        'dropout_keep_prob': 0.8,
        'goal': 'ranking',
        'addBias': False,
        'with_review_input': True,
        'attention_version': 'new',
        'lam1': 0.0,
        'verbose': False,
        'item_pad_num': 8,
        'if_full_review': True
    }
    dataModel = src.data_model.ReviewDataModel.ReviewDataModel(config)
    dataModel.buildModel()

    # for learnrate in [0.0001, 0.001, 0.01, 0.1]:
    #     for batchSize in [2000, 1000, 500, 250, 128]:
    #         config['learnRate'] = learnrate
    #         config['trainBatchSize'] = batchSize
    #         rec = recommender.ReviewBPR_CNN.ReviewBPR_CNN(dataModel, config)
    #         rec.run()

    for lam in [0.0001, 0.001, 0.01, 0.1, 1]:
        for dropout in [1.0, 0.9, 0.8, 0.7, 0.6]:
            config['factor_lambda'] = lam
            config['cnn_lambda'] = lam
            config['atten_lambda'] = lam
            config['dropout_keep_prob'] = dropout
            rec = recommender.ReviewBPR_CNN.ReviewBPR_CNN(dataModel, config)
            rec.run()









# 1.0, 0.9, 0.8, 0.7, 0.6, 0.5







