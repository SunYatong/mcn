from recommender.MCN_vector import MUIC_Recommender
from data_model.SequenceDataModel import SequenceDataModel

# os.environ["CUDA_DEVICE_ORDER"]="PCI_BUS_ID"   # see issue #152
# os.environ[""]="0"


if __name__ == '__main__':

    # instant_video_5_core, digital_music_5_core_100, baby_5_core_100  apps_for_android_5 video_games_5

    # ml-100k: 2 1 2 r=3
    # elec: 1 2 1 r=1
    # kindle: 1 2 2 r=1

    config = {
        'test-info': 'MCN',
        'generate_seq': True,
        'splitterType': 'userTimeRatio',
        'fileName': 'ml-100k',
        'trainType': 'test',
        'threshold': 0,
        'learnRate': 0.001,
        'maxIter': 1000,
        'trainBatchSize': 939,  # 830,
        'testBatchSize': 939,  # 830,
        'numFactor': 16,
        'topN': 5,
        'factor_lambda': 0.01,
        'goal': 'ranking',
        'verbose': False,
        'input_length': 3,
        'target_length': 1,
        'dropout_keep': 0.8,
        'item_fc_dim': [16],
        'capsule_num': 1,
        'dynamic_routing_iter': 3,
        'eval_item_num': 100,
        'filter_num': 5,
        'early_stop': True,

        'capsule_lambda': 0.01,
        'cnn_lam': 0.001,
        'mlp_lam': 0.001,
        'capsule_lam': 0.001,
        'save_model': False,
        'load_model': False,

        'random_seed': 1,

        'item-weight-more': False,
        'corr-type': 'dot-product'
    }

    for fileName in ['ml-100k']:
        config['fileName'] = fileName

        dataModel = SequenceDataModel(config)
        dataModel.buildModel()

        for input_len in [5]:
            for target_len in [1]:

                dataModel.generate_sequences_hor(input_len, target_len)
                config['input_length'] = input_len
                config['target_length'] = target_len

                for capsule_lam in [0.001]:
                    for T in [1]:
                        for capsule_num_1 in [1]:
                            for capsule_num_2 in [1]:
                                for capsule_num_3 in [1]:
                                    config['capsule_lambda'] = capsule_lam
                                    config['dynamic_routing_iter'] = T
                                    config['user_item_capsule_num'] = capsule_num_1
                                    config['item_item_capsule_num'] = capsule_num_2
                                    config['preference_capsule_num'] = capsule_num_3
                                    recommender = MUIC_Recommender(dataModel, config)
                                    recommender.run()









