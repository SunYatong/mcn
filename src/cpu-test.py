from __future__ import print_function
from collections import OrderedDict
import pprint

def CPUinfo():
    '''Return the info in /proc/cpuinfo
    as a dirctionary in the follow format:
    CPU_info['proc0']={...}
    CPU_info['proc1']={...}
    '''

    CPUinfo=OrderedDict()
    procinfo=OrderedDict()

    nprocs = 0
    with open('/proc/cpuinfo') as f:
        for line in f:
            if not line.strip():
                #end of one processor
                CPUinfo['proc%s' % nprocs]=procinfo
                nprocs = nprocs+1
                #Reset
                procinfo=OrderedDict()
            else:
                if len(line.split(':')) == 2:
                    procinfo[line.split(':')[0].strip()] = line.split(':')[1].strip()
                else:
                    procinfo[line.split(':')[0].strip()] = ''
    return CPUinfo

def preprocess_data(file):
    losses = []
    ndcgs = []

    if len(file.split('ml100k')) > 1:
        ndcg_factor = 0.5628 / 0.4347
        loss_factor = 10
    else:
        ndcg_factor = 1.0
        loss_factor = 1.0

    with open(file) as input_file:
        for line in input_file:
            words = line.strip().split(' ')
            if 'totalLoss:' in words:
                losses.append(float(words[-1]) * loss_factor)

            if 'NDCG' in words and 'best' in words:
                for i in range(5):
                    ndcgs.append(float(words[5].split(',')[0]) * ndcg_factor)
            if 'NDCG@10:' in words:
                for i in range(5):
                    ndcgs.append(float(words[3].split(',')[0].split('[')[1]) * ndcg_factor)

    return losses, ndcgs

font_size = 28
labelsize = font_size - 10
linewidth = 4.0


def draw_trainloss_diagram(dataset):
    import matplotlib.pyplot as plt

    plt.rcParams['figure.figsize'] = (10.0, 8.0)
    plt.rc('xtick', labelsize=labelsize)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=labelsize)  # fontsize of the tick labels

    losses_mcn, ndcg_mcn = preprocess_data('./diagram_log/mcn_' + dataset + '.log')
    x3 = range(len(losses_mcn))
    y3 = losses_mcn
    plt.plot(x3, y3, label='MCN', c='red', linewidth=linewidth, linestyle='-')

    losses_matrix, ndcgs = preprocess_data('./diagram_log/mcn_matrix_' + dataset + '.log')
    x1 = range(len(losses_matrix))
    y1 = losses_matrix
    plt.plot(x1, y1, label='MCN_matrix', c='blue', linewidth=linewidth, linestyle='-.')

    losses_vector, ndcg_vector = preprocess_data('./diagram_log/mcn_vector_' + dataset + '.log')
    x2 = range(len(losses_vector))
    y2 = losses_vector
    plt.plot(x2, y2, label='MCN_vector', c='green', linewidth=linewidth, linestyle=':')



    plt.legend(loc='top right', fontsize=font_size)  # 让图例生效

    plt.xlabel("epoch number", fontsize=font_size)  # X轴标签
    plt.ylabel("training loss", fontsize=font_size)  # Y轴标签

    plt.ticklabel_format(style='sci', axis='y', scilimits=(0,0))

    ax = plt.axes()
    ax.yaxis.grid(ls='--')

    plt.savefig('./save_fig/' + dataset + '-train.png')

    plt.show()


def draw_testndcg_diagram(dataset):
    import matplotlib.pyplot as plt

    plt.rcParams['figure.figsize'] = (10.0, 8.0)
    plt.rc('xtick', labelsize=labelsize)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=labelsize)  # fontsize of the tick labels

    epoch_range = 400

    losses_mcn, ndcg_mcn = preprocess_data('./diagram_log/mcn_' + dataset + '.log')
    x3 = range(len(ndcg_mcn))
    y3 = ndcg_mcn
    plt.plot(x3, y3, label='MCN', c='red', linewidth=linewidth, linestyle='-')

    losses_matrix, ndcgs_matrix = preprocess_data('./diagram_log/mcn_matrix_' + dataset + '.log')
    x1 = range(len(ndcgs_matrix))
    y1 = ndcgs_matrix
    plt.plot(x1, y1, label='MCN_matrix', c='blue', linewidth=linewidth, linestyle='-.')

    losses_vector, ndcg_vector = preprocess_data('./diagram_log/mcn_vector_' + dataset + '.log')
    x2 = range(len(ndcg_vector))
    y2 = ndcg_vector
    plt.plot(x2, y2, label='MCN_vector', c='green', linewidth=linewidth, linestyle=':')

    plt.legend(loc='bottom right', fontsize=font_size)  # 让图例生效

    plt.xlabel("epoch number", fontsize=font_size)  # X轴标签
    plt.ylabel("NDCG@10", fontsize=font_size)  # Y轴标签

    ax = plt.axes()
    ax.yaxis.grid(ls='--')

    plt.savefig('./save_fig/' + dataset + '-test.png')

    plt.show()



def parameter_analysis(dataset, parameter_name):
    import matplotlib.pyplot as plt

    plt.rcParams['figure.figsize'] = (10.0, 8.0)
    plt.rc('xtick', labelsize=labelsize)  # fontsize of the tick labels
    plt.rc('ytick', labelsize=labelsize)  # fontsize of the tick labels

    parameter_values = ["1", "2", "3", "4", "5"]
    if parameter_name == 'T':
        ml1m_NDCGs = [0.5017, 0.5154, 0.5218, 0.5207, 0.5110]
        elec_NDCGs = [0.3408, 0.3524, 0.3568, 0.3537, 0.3429]
        kindle_NDCGs = [0.4603, 0.4689, 0.4705, 0.4698, 0.4637]
    else:
        ml1m_NDCGs = [0.5006, 0.5184, 0.5218, 0.5217, 0.5216]
        elec_NDCGs = [0.3410, 0.3568, 0.3558, 0.3560, 0.3558]
        kindle_NDCGs = [0.4593, 0.4705, 0.4700, 0.4688, 0.4677]

    plt.plot(parameter_values, ml1m_NDCGs, label='ml1m', c='red', linewidth=linewidth, marker='o', markersize=font_size - 10)
    plt.plot(parameter_values, elec_NDCGs, label='elect', c='blue', linewidth=linewidth, marker='^', markersize=font_size - 10)
    plt.plot(parameter_values, kindle_NDCGs, label='kindle', c='green', linewidth=linewidth, marker='s', markersize=font_size - 10)

    plt.legend(fontsize=font_size, loc='upper right', bbox_to_anchor=(1.0, 0.6))  # 让图例生效

    plt.xlabel(parameter_name, fontsize=font_size)  # X轴标签
    plt.ylabel("NDCG@5", fontsize=font_size)  # Y轴标签

    plt.xticks(range(len(parameter_values)), parameter_values, size=labelsize)

    ax = plt.axes()
    ax.yaxis.grid(ls='--')

    plt.savefig('./save_fig/' + dataset + '-' + str(parameter_name) + '.png')

    plt.show()

if __name__ == '__main__':
    parameter_analysis('all', "T")
    # CPUinfo = CPUinfo()
    # for processor in CPUinfo.keys():
    #     print('CPUinfo[{0}]={1}'.format(processor,CPUinfo[processor]['model name']))

    # a = 0.6063
    # # b = 0.7052
    # # #
    # # print ((a-b) / b * 100)
    #
    # imp = 2.99
    #
    #
    # print((100 * a) / (100 + imp))

    # numUser = 6040
    # numItem = 3705
    # numRating = 1000207
    #
    # density = numRating / (numUser * numItem) * 100
    # print(density)
    #
    # draw_trainloss_diagram(dataset='ml100k')
    # draw_testndcg_diagram(dataset='ml100k')
